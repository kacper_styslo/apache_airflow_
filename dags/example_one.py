# PSL
import sys
import socket
from datetime import datetime
from datetime import timedelta
from typing import NoReturn

try:
    # Third part
    from airflow import DAG
    from airflow.operators.python_operator import PythonOperator

    print("All Dag modules are ok ......")
except Exception as err:
    print(f"Error  {err} ")


def get_host_ip_address() -> str:
    raw_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    raw_sock.connect(("8.8.8.8", 80))
    host_ip: str = raw_sock.getsockname()[0]
    raw_sock.close()
    return host_ip


def catch_host_ip(**kwargs) -> NoReturn:
    print("Getting host ip address . . .")
    kwargs["ti"].xcom_push(key="host_ip", value=kwargs["host_ip"])


def show_host_ip_address(**kwargs) -> str:
    _host_ip = kwargs.get("ti").xcom_pull(key="host_ip")
    return f"Host IP address: {_host_ip}"

with DAG(
        dag_id="dag_one",
        schedule_interval="@daily",
        default_args={
            "owner": "airflow",
            "retries": 1,
            "retry_delay": timedelta(minutes=5),
            "start_date": datetime(2021, 1, 1),
        },
        catchup=False
) as _dag:
    catch_host_ip = PythonOperator(task_id="catch_host_ip", python_callable=catch_host_ip,
                                   provide_context=True,
                                   op_kwargs={"host_ip": get_host_ip_address()})

    show_host_ip_address = PythonOperator(task_id="show_host_ip_address",
                                          python_callable=show_host_ip_address,
                                          provide_context=True)

catch_host_ip >> show_host_ip_address
